#include <stdio.h>
int fibonacciSeq(int sequence_rounds)
{
    if(sequence_rounds == 1) {
      return 0;
   }
	
   if(sequence_rounds == 2) {
      return 1;
   }
	return fibonacciSeq(sequence_rounds-1) + fibonacciSeq(sequence_rounds-2);
}
int main()
{
    int sequence_rounds;
    printf("Enter the Number of times function wants to run : ");
    scanf("%d",&sequence_rounds);
    if(sequence_rounds==1)
        {
            printf("0");
        }
    if(sequence_rounds==2)
    {
        printf("0\n1");
    }
    if(sequence_rounds>2)
    {
    for(int i=1;i<=sequence_rounds+1;i++)
        {
            printf("%d",fibonacciSeq(i));
            printf("\n");
        }
    }
    return 0;
}